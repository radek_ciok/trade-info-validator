# README #

Application is packed as jar and should be run as standard java application with main class
rc.apps.tradeinfovalidator.TradeInfoValidatorApplication

After start web GUI will be available under link
http://localhost:8080/trade-info-validator/tradeinfo

should be compile and run with java 8

start with cli
java -jar target/trade-info-validator-0.0.1-SNAPSHOT.jar

validation requests:
- spot
POST http://localhost:8080/trade-info-validator/validation/spot

- forward
POST http://localhost:8080/trade-info-validator/validation/forward

- option
POST http://localhost:8080/trade-info-validator/validation/option

- shutdown
POST http://localhost:8080/trade-info-validator/shutdown