package rc.apps.tradeinfovalidator.web.validation.rules;

import java.time.LocalDate;

public class IsExpiryDateValidRule  extends TradeInfoValidationRule {

    private LocalDate expiryDate;
    private LocalDate premiumDate;
    private LocalDate deliveryDate;
    
    public IsExpiryDateValidRule(String fieldName, LocalDate expiryDate, LocalDate premiumDate, LocalDate deliveryDate) {
        super(fieldName);
        this.expiryDate = expiryDate;
        this.premiumDate = premiumDate;
        this.deliveryDate = deliveryDate;
    }

    @Override
    public boolean isBroken() {
        return !( expiryDate.isBefore(deliveryDate) && premiumDate.isBefore(deliveryDate) );
    }

    @Override
    public String getMessage() {
        return "Expiry date and premium date shall be before delivery date";
    }

}
