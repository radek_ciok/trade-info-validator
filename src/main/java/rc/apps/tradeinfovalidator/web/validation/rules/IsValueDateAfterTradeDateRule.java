package rc.apps.tradeinfovalidator.web.validation.rules;

import java.time.LocalDate;

public class IsValueDateAfterTradeDateRule extends TradeInfoValidationRule {

	private LocalDate valueDate; 
	private LocalDate tradeDate;

	public IsValueDateAfterTradeDateRule(String fieldName, LocalDate valueDate, LocalDate tradeDate) {
		super(fieldName);
		this.valueDate = valueDate;
		this.tradeDate = tradeDate;
	}

	@Override
	public boolean isBroken() {
		return valueDate.isBefore(tradeDate);
	}
	
	@Override
	public String getMessage() {
		return "Value Date have to be after Trade Date";
	}

}
