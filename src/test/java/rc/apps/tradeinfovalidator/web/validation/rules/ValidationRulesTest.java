package rc.apps.tradeinfovalidator.web.validation.rules;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

public class ValidationRulesTest {
	
	private static int DAYS_AFTER_CURRENT_DATE = 2;
	
	@Test public void testSupportedCounterpartyRule() {
		assertFalse(new IsSupportedCounterpartyRule("customer", "JUPITER1").isBroken());
		assertTrue(new IsSupportedCounterpartyRule("customer", "CUST1").isBroken());
		assertTrue(new IsSupportedCounterpartyRule("customer", "").isBroken());
	}
	
	@Test public void testSupportedStylesRule() {
		assertFalse(new IsSupportedStyleRule("style", "AMERICAN").isBroken());
		assertFalse(new IsSupportedStyleRule("style", "EUROPEAN").isBroken());
		assertTrue(new IsSupportedStyleRule("style", "NOT_SUPP_STYLE").isBroken());
		assertTrue(new IsSupportedStyleRule("style", "").isBroken());
	}
	
	@Test public void testValueTwoDaysAfterCurrentDate() {
		assertFalse(new IsValueDaysAfterCurrentDateRule("valueDate", LocalDate.of(2017, 6, 7), DAYS_AFTER_CURRENT_DATE).isBroken());
		assertTrue(new IsValueDaysAfterCurrentDateRule("valueDate", LocalDate.of(2017, 6, 6), DAYS_AFTER_CURRENT_DATE).isBroken());
		assertTrue(new IsValueDaysAfterCurrentDateRule("valueDate", LocalDate.of(2017, 6, 8), DAYS_AFTER_CURRENT_DATE).isBroken());
	}
	
	@Test public void ValidISOCodeRule() {
		assertFalse( new IsValidISOCodeRule("currencyCode", "USDEUR").isBroken() );
		assertFalse( new IsValidISOCodeRule("currencyCode", "USDUSD").isBroken() );
		
		assertTrue( new IsValidISOCodeRule("currencyCode", "usdeur").isBroken() );
		assertTrue( new IsValidISOCodeRule("currencyCode", "USD").isBroken() );
		assertTrue( new IsValidISOCodeRule("currencyCode", "USDEUX").isBroken() );
		assertTrue( new IsValidISOCodeRule("currencyCode", "").isBroken() );
		assertTrue( new IsValidISOCodeRule("currencyCode", "USDEURUSDEUR").isBroken() );
	}
	
	@Test public void testExcerciseDateValidRule() {
		
	}
	
	@Test public void testExpiryDateValidRule() {
		
	}
	
	@Test public void ValueDateAfterTradeDateRule() {}
	
	@Test public void ValueDaysAfterCurrentDateRule() {}
	
	@Test public void WorkingDayRule() {}
}
