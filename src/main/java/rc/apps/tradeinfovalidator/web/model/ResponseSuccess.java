package rc.apps.tradeinfovalidator.web.model;

public class ResponseSuccess {

	private String message;
	
	public ResponseSuccess(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
