package rc.apps.tradeinfovalidator.web.model;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import rc.apps.tradeinfovalidator.web.validation.ValidOption;

@ValidOption
public class Option extends CommonTradeInfo {
    
    private String strategy;
    
    @NotNull
    private LocalDate deliveryDate;
    
    private LocalDate excerciseDate;

    @NotNull
    private LocalDate expiryDate;
    
    private String payCcy;
    
    private Float premium;
    
    private String premiumCcy;
    
    private String premiumType;
    
    @NotNull
    private String style;
    
    @NotNull
    private LocalDate premiumDate;

	public String getStrategy() {
		return strategy;
	}

	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}

	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDate deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public LocalDate getExcerciseDate() {
		return excerciseDate;
	}

	public void setExcerciseDate(LocalDate excerciseDate) {
		this.excerciseDate = excerciseDate;
	}

	public LocalDate getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getPayCcy() {
		return payCcy;
	}

	public void setPayCcy(String payCcy) {
		this.payCcy = payCcy;
	}

	public Float getPremium() {
		return premium;
	}

	public void setPremium(Float premium) {
		this.premium = premium;
	}

	public String getPremiumCcy() {
		return premiumCcy;
	}

	public void setPremiumCcy(String premiumCcy) {
		this.premiumCcy = premiumCcy;
	}

	public String getPremiumType() {
		return premiumType;
	}

	public void setPremiumType(String premiumType) {
		this.premiumType = premiumType;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public LocalDate getPremiumDate() {
		return premiumDate;
	}

	public void setPremiumDate(LocalDate premiumDate) {
		this.premiumDate = premiumDate;
	}

	@Override
	public String toString() {
		return "Option [strategy=" + strategy + ", deliveryDate=" + deliveryDate + ", excerciseDate=" + excerciseDate
				+ ", expiryDate=" + expiryDate + ", payCcy=" + payCcy + ", premium=" + premium + ", premiumCcy="
				+ premiumCcy + ", premiumType=" + premiumType + ", style=" + style + ", premiumDate=" + premiumDate
				+ "]";
	}

   
}
