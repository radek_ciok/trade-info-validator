$(function() {

	function clear_err_messages(err_class) {
    	$("." + err_class).each(function( index ) {
  		  $( this ).text("");
    	});		
	}

	function set_validation_status(prefix, isValid) {
		if (isValid) {
			$("#" + prefix + "status").text("Validation success");
			$("#" + prefix + "status").css("color", "green");
		} else {
			$("#" + prefix + "status").text("Validation failed");
			$("#" + prefix + "status").css("color", "red");
		}
	}

	function set_shutdown_status(prefix, isShutdown) {
		if (isShutdown) {
			$("input").prop('disabled', true);
			$("button").prop('disabled', true);
			$("#" + prefix + "status").text("Shutdown success");
			$("#" + prefix + "status").css("color", "green");
		} else {
			$("input").prop('disabled', false);
			$("button").prop('disabled', false);
			$("#" + prefix + "status").text("Shutdown failed");
			$("#" + prefix + "status").css("color", "red");
		}
	}

	
	// ======================================================= SPOT =============================================================
	
	$( "#spot_validate" ).click(function() {
		console.log("data to validate: ");

		var type_prefix = "spot_";

		var data = {
				"customer": $("#" + type_prefix + "customer").val(),	
				"ccyPair": $("#" + type_prefix + "ccyPair").val(),	
				"type": "Spot",	
				"direction": $("#" + type_prefix + "direction").val(),	
				"tradeDate": $("#" + type_prefix + "tradeDate").val(),	
				"amount1": $("#" + type_prefix + "amount1").val(),	
				"amount2": $("#" + type_prefix + "amount2").val(),	
				"rate": $("#" + type_prefix + "rate").val(),	
				"valueDate": $("#" + type_prefix + "valueDate").val(),	
				"legalEntity": $("#" + type_prefix + "legalEntity").val(),	
				"trader": $("#" + type_prefix + "trader").val()
			};
			
			var strData = JSON.stringify(data);
			
			$.ajax({
			    type: 'POST',
			    url: 'http://localhost:8080/trade-info-validator/validation/spot',
			    data: strData, 
			    success: function(data) { 
			    	set_validation_status(type_prefix, true);
			    	clear_err_messages("error_msg_spot");
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			    	set_validation_status(type_prefix, false);
			    	clear_err_messages("error_msg_spot");
			    	jqXHR.responseJSON.forEach(function(item, index) {
			    		var newMsg = $("#" + type_prefix + "err_" + item.field).text() + " | " + item.defaultMessage;
			    		$("#" + type_prefix + "err_" + item.field).text(newMsg);
			    	});
			    	
			    },
			    contentType: "application/json",
			    dataType: 'json'
			});

			
		});

	// ======================================================= FORWARD =============================================================
	
	$( "#forward_validate" ).click(function() {
		console.log("data to validate: ");

		var type_prefix = "forward_";

		var data = {
				"customer": $("#" + type_prefix + "customer").val(),	
				"ccyPair": $("#" + type_prefix + "ccyPair").val(),	
				"type": "Forward",	
				"direction": $("#" + type_prefix + "direction").val(),	
				"tradeDate": $("#" + type_prefix + "tradeDate").val(),	
				"amount1": $("#" + type_prefix + "amount1").val(),	
				"amount2": $("#" + type_prefix + "amount2").val(),	
				"rate": $("#" + type_prefix + "rate").val(),	
				"valueDate": $("#" + type_prefix + "valueDate").val(),	
				"legalEntity": $("#" + type_prefix + "legalEntity").val(),	
				"trader": $("#" + type_prefix + "trader").val()
			};
			
			var strData = JSON.stringify(data);
			
			$.ajax({
			    type: 'POST',
			    url: 'http://localhost:8080/trade-info-validator/validation/forward',
			    data: strData, 
			    success: function(data) { 
			    	set_validation_status(type_prefix, true);
			    	clear_err_messages("error_msg_forward");
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			    	set_validation_status(type_prefix, false);
			    	clear_err_messages("error_msg_forward");
			    	jqXHR.responseJSON.forEach(function(item, index) {
			    		var newMsg = $("#" + type_prefix + "err_" + item.field).text() + " | " + item.defaultMessage;
			    		$("#" + type_prefix + "err_" + item.field).text(newMsg);
			    	});
			    	
			    },
			    contentType: "application/json",
			    dataType: 'json'
			});
			
		});
	
// ======================================================= OPTIONS =============================================================
	
	$( "#options_validate" ).click(function() {
		console.log("data to validate: ");

		var type_prefix = "options_";

		var data = {
				"customer": $("#" + type_prefix + "customer").val(),	
				"ccyPair": $("#" + type_prefix + "ccyPair").val(),	
				"type": "VanillaOption",	
				"style": $("#" + type_prefix + "style").val(),	
				"excerciseDate": $("#" + type_prefix + "excerciseDate").val(),	
				"direction": $("#" + type_prefix + "direction").val(),	
				"strategy": $("#" + type_prefix + "strategy").val(),	
				"tradeDate": $("#" + type_prefix + "tradeDate").val(),	
				"amount1": $("#" + type_prefix + "amount1").val(),	
				"amount2": $("#" + type_prefix + "amount2").val(),	
				"rate": $("#" + type_prefix + "rate").val(),	
				"deliveryDate": $("#" + type_prefix + 	"deliveryDate").val(),	
				"expiryDate": $("#" + type_prefix + 	"expiryDate").val(),	
				"payCcy": $("#" + type_prefix + 		"payCcy").val(),	
				"premium": $("#" + type_prefix + 		"premium").val(),	
				"premiumCcy": $("#" + type_prefix + 	"premiumCcy").val(),	
				"premiumType": $("#" + type_prefix + 	"premiumType").val(),	
				"premiumDate": $("#" + type_prefix + 	"premiumDate").val(),	
				"legalEntity": $("#" + type_prefix + "legalEntity").val(),	
				"trader": $("#" + type_prefix + "trader").val()
			};
			
			var strData = JSON.stringify(data);
			
			$.ajax({
			    type: 'POST',
			    url: 'http://localhost:8080/trade-info-validator/validation/option',
			    data: strData, 
			    success: function(data) { 
			    	set_validation_status(type_prefix, true);
			    	clear_err_messages("error_msg_options");
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			    	set_validation_status(type_prefix, false);
			    	clear_err_messages("error_msg_options");
			    	jqXHR.responseJSON.forEach(function(item, index) {
			    		var newMsg = $("#" + type_prefix + "err_" + item.field).text() + " | " + item.defaultMessage;
			    		$("#" + type_prefix + "err_" + item.field).text(newMsg);
			    	});
			    },
			    contentType: "application/json",
			    dataType: 'json'
			});

			
		});
	
// ======================================================= SHUTDOWN =============================================================
	
	$( "#shutdown" ).click(function() {

		$.ajax({
		    type: 'POST',
		    url: 'http://localhost:8080/trade-info-validator/shutdown',
		    success: function() { 
		    	set_shutdown_status("shutdown_", true);
		    },
		    error: function (jqXHR, textStatus, errorThrown)
		    {
		    	set_shutdown_status("shutdown_", true);
		    }
		});
		
	});

		
});