package rc.apps.tradeinfovalidator.web.validation.rules;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class IsWorkingDayRule extends TradeInfoValidationRule {

	private LocalDate date;
	
	public IsWorkingDayRule(String fieldName, LocalDate date) {
		super(fieldName);
		this.date = date;
	}

	@Override
	public boolean isBroken() {
		return date.getDayOfWeek() == DayOfWeek.SATURDAY || date.getDayOfWeek() == DayOfWeek.SUNDAY;
	}

	@Override
	public String getMessage() {
		return "Date have to be a working day";
	}

}
