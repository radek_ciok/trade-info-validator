package rc.apps.tradeinfovalidator.web.validation.rules;

import java.time.LocalDate;

public class IsValueDaysAfterCurrentDateRule extends TradeInfoValidationRule {

	private LocalDate date;
	private int allowedDaysAfter;
	
	public IsValueDaysAfterCurrentDateRule(String fieldName, LocalDate date, int allowedDaysAfter) {
		super(fieldName);
		this.date = date;
		this.allowedDaysAfter = allowedDaysAfter;
	}

	@Override
	public boolean isBroken() {
		LocalDate currentDate = LocalDate.of(2017, 6, 5);
		return !currentDate.isEqual(date.minusDays(allowedDaysAfter));
	}

	@Override
	public String getMessage() {
		return "Value date for this type have to be 2 days after current date";
	}

}
