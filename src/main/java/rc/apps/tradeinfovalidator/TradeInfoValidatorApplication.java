package rc.apps.tradeinfovalidator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradeInfoValidatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradeInfoValidatorApplication.class, args);
	}
}
