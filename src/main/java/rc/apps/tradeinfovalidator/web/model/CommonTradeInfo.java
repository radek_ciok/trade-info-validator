package rc.apps.tradeinfovalidator.web.model;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

public class CommonTradeInfo {

	@NotNull
	private String customer;
	
	@NotNull
	private String ccyPair;
	
	private TradeInfoType type;
	
	private Direction direction;
	
	@NotNull
	private LocalDate tradeDate;
	
	private Double amount1;
	
	private Double amount2;
	
	private Float rate;
	
	private String legalEntity;
	
	private String trader;
	
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getCcyPair() {
		return ccyPair;
	}
	public void setCcyPair(String ccyPair) {
		this.ccyPair = ccyPair;
	}
	public TradeInfoType getType() {
		return type;
	}
	public void setType(TradeInfoType type) {
		this.type = type;
	}
	public Direction getDirection() {
		return direction;
	}
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	public LocalDate getTradeDate() {
		return tradeDate;
	}
	public void setTradeDate(LocalDate tradeDate) {
		this.tradeDate = tradeDate;
	}
	public Double getAmount1() {
		return amount1;
	}
	public void setAmount1(Double amount1) {
		this.amount1 = amount1;
	}
	public Double getAmount2() {
		return amount2;
	}
	public void setAmount2(Double amount2) {
		this.amount2 = amount2;
	}
	public Float getRate() {
		return rate;
	}
	public void setRate(Float rate) {
		this.rate = rate;
	}
	public String getLegalEntity() {
		return legalEntity;
	}
	public void setLegalEntity(String legalEntity) {
		this.legalEntity = legalEntity;
	}
	public String getTrader() {
		return trader;
	}
	public void setTrader(String trader) {
		this.trader = trader;
	}


}
