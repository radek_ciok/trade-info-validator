package rc.apps.tradeinfovalidator.web.model;

public enum Direction {
	BUY, SELL
}
