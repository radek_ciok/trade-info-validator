package rc.apps.tradeinfovalidator.web.validation;

import java.util.List;

import rc.apps.tradeinfovalidator.web.model.Spot;
import rc.apps.tradeinfovalidator.web.model.CommonTradeInfo;
import rc.apps.tradeinfovalidator.web.validation.rules.IsSupportedCounterpartyRule;
import rc.apps.tradeinfovalidator.web.validation.rules.IsValidISOCodeRule;
import rc.apps.tradeinfovalidator.web.validation.rules.IsValueDateAfterTradeDateRule;
import rc.apps.tradeinfovalidator.web.validation.rules.IsValueDaysAfterCurrentDateRule;
import rc.apps.tradeinfovalidator.web.validation.rules.IsWorkingDayRule;
import rc.apps.tradeinfovalidator.web.validation.rules.TradeInfoValidationRule;

public class SpotValidator extends TradeoInfoValidator<ValidSpot>{

	@Override
	protected void addValidationRules(CommonTradeInfo tradeInfo, List<TradeInfoValidationRule> rules) {
		Spot spot = (Spot) tradeInfo;
		rules.add(new IsValueDateAfterTradeDateRule("valueDate", spot.getValueDate(), spot.getTradeDate()));		
		rules.add(new IsWorkingDayRule("valueDate", spot.getValueDate()));
		rules.add(new IsSupportedCounterpartyRule("customer", spot.getCustomer()));
		rules.add(new IsValidISOCodeRule("ccyPair", spot.getCcyPair()));	
		rules.add(new IsValueDaysAfterCurrentDateRule("valueDate", spot.getValueDate(), 2));
	}


}
