package rc.apps.tradeinfovalidator.web.validation;

import java.util.List;

import rc.apps.tradeinfovalidator.web.model.Option;
import rc.apps.tradeinfovalidator.web.model.CommonTradeInfo;
import rc.apps.tradeinfovalidator.web.validation.rules.IsExcerciseDateValidRule;
import rc.apps.tradeinfovalidator.web.validation.rules.IsExpiryDateValidRule;
import rc.apps.tradeinfovalidator.web.validation.rules.IsSupportedCounterpartyRule;
import rc.apps.tradeinfovalidator.web.validation.rules.IsSupportedStyleRule;
import rc.apps.tradeinfovalidator.web.validation.rules.IsValidISOCodeRule;
import rc.apps.tradeinfovalidator.web.validation.rules.TradeInfoValidationRule;

public class OptionValidator extends TradeoInfoValidator<ValidOption> {

	@Override
	protected void addValidationRules(CommonTradeInfo tradeInfo, List<TradeInfoValidationRule> rules) {
		Option option = (Option) tradeInfo;
		rules.add( new IsSupportedCounterpartyRule("customer", option.getCustomer()) );
		rules.add( new IsValidISOCodeRule("ccyPair", option.getCcyPair()) );
		rules.add( new IsSupportedStyleRule("style", option.getStyle()) );
		rules.add( new IsExcerciseDateValidRule("excerciseDate", option.getExcerciseDate(), option.getTradeDate(), option.getExpiryDate(), option.getStyle()) );
		rules.add( new IsExpiryDateValidRule("expiryDate", option.getExpiryDate(), option.getPremiumDate(), option.getDeliveryDate()) );
		
	}

}
