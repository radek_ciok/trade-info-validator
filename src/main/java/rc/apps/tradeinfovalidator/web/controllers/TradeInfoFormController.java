package rc.apps.tradeinfovalidator.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TradeInfoFormController {

	@RequestMapping("/tradeinfo")
	public String showTradeInfoForm() {
		System.out.println("showing trade information form");
		return "trade-info-form";
	}
	
}
