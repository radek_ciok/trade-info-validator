package rc.apps.tradeinfovalidator.web.validation;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import rc.apps.tradeinfovalidator.web.model.CommonTradeInfo;
import rc.apps.tradeinfovalidator.web.validation.rules.TradeInfoValidationRule;

public abstract class TradeoInfoValidator<A extends Annotation> implements ConstraintValidator<A, CommonTradeInfo> {

	private List<TradeInfoValidationRule> rules; 
	
	@Override
	public void initialize(A constraintAnnotation) {
	}

	@Override
	public boolean isValid(CommonTradeInfo value, ConstraintValidatorContext context) {
		boolean isValid = true;
		rules = new ArrayList<TradeInfoValidationRule>();
		addValidationRules(value, rules);
		context.disableDefaultConstraintViolation();
		
		for (TradeInfoValidationRule rule : rules) {
			if (rule.isBroken()) {
				context.buildConstraintViolationWithTemplate(rule.getMessage())
                .addPropertyNode(rule.getFieldName()).addConstraintViolation();
				isValid = false;	
			}
		}
		
		return isValid;
	}
	
	abstract protected void addValidationRules(CommonTradeInfo tradeInfo, List<TradeInfoValidationRule> rules);

}
