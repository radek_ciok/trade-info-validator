package rc.apps.tradeinfovalidator.web.validation.rules;

public abstract class TradeInfoValidationRule {
	
	private String fieldName;
	
	public abstract boolean isBroken();
	
	public abstract String getMessage();
	
	public TradeInfoValidationRule(String fieldName) {
		this.fieldName = fieldName;
	}
	
	public String getFieldName() {
		return fieldName;
	}
}
