package rc.apps.tradeinfovalidator.web.validation;

import java.util.List;

import rc.apps.tradeinfovalidator.web.model.Forward;
import rc.apps.tradeinfovalidator.web.model.CommonTradeInfo;
import rc.apps.tradeinfovalidator.web.validation.rules.IsSupportedCounterpartyRule;
import rc.apps.tradeinfovalidator.web.validation.rules.IsValidISOCodeRule;
import rc.apps.tradeinfovalidator.web.validation.rules.IsValueDateAfterTradeDateRule;
import rc.apps.tradeinfovalidator.web.validation.rules.IsWorkingDayRule;
import rc.apps.tradeinfovalidator.web.validation.rules.TradeInfoValidationRule;

public class ForwardValidator extends TradeoInfoValidator<ValidForward>{

	@Override
	protected void addValidationRules(CommonTradeInfo tradeInfo, List<TradeInfoValidationRule> rules) {
		Forward forward = (Forward) tradeInfo;
		rules.add(new IsValueDateAfterTradeDateRule("valueDate", forward.getValueDate(), forward.getTradeDate()));		
		rules.add(new IsWorkingDayRule("valueDate", forward.getValueDate()));
		rules.add(new IsSupportedCounterpartyRule("customer", forward.getCustomer()));
		rules.add(new IsValidISOCodeRule("ccyPair", forward.getCcyPair()));
	}

}
