package rc.apps.tradeinfovalidator.web.validation.rules;

public class IsSupportedStyleRule extends TradeInfoValidationRule {
    
    private String style;
    
    public IsSupportedStyleRule(String fieldName, String style) {
        super(fieldName);
        this.style = style;
    }

    @Override
    public boolean isBroken() {
        return !SupportedStyles.isSupported(style);
    }

    @Override
    public String getMessage() {
        return "This style is not supported";
    }
    
}
