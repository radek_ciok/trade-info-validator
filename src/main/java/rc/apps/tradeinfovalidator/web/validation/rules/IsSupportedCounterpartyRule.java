package rc.apps.tradeinfovalidator.web.validation.rules;

import java.util.Arrays;
import java.util.List;

public class IsSupportedCounterpartyRule extends TradeInfoValidationRule {

	private final List<String> supportedCounterparties = Arrays.asList("JUPITER1", "JUPITER2");
	
	private String counterparty;
	
	public IsSupportedCounterpartyRule(String fieldName, String counterparty) {
		super(fieldName);
		this.counterparty = counterparty;
	}

	@Override
	public boolean isBroken() {
		return !supportedCounterparties.contains(counterparty);
	}

	@Override
	public String getMessage() {
		return "This counterparty is not supported";
	}

}
