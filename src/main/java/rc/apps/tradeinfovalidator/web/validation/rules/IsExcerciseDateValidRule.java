package rc.apps.tradeinfovalidator.web.validation.rules;

import java.time.LocalDate;
import java.util.Date;

public class IsExcerciseDateValidRule extends TradeInfoValidationRule {

    private LocalDate excerciseDate;
    private LocalDate tradeDate;
    private LocalDate expiryDate;
    private SupportedStyles style;
    
    public IsExcerciseDateValidRule(String fieldName, LocalDate excerciseDate, LocalDate tradeDate, LocalDate expiryDate, String style) {
        super(fieldName);
        this.excerciseDate = excerciseDate;
        this.style = SupportedStyles.getStyleByName(style);
        this.tradeDate = tradeDate;
        this.expiryDate = expiryDate;
    }

    @Override
    public boolean isBroken() {
        if (style == SupportedStyles.AMERICAN) {
            if (excerciseDate == null) {
                return true;
            }
            return !(excerciseDate.isAfter(tradeDate) && excerciseDate.isBefore(expiryDate));
        } 
        return false;            
    }

    @Override
    public String getMessage() {
        return "American style requires excerciseDate, which is after the trade date and before the expiry date";
    }

}
