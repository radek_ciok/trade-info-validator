package rc.apps.tradeinfovalidator.web.rest_services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import rc.apps.tradeinfovalidator.web.model.Forward;
import rc.apps.tradeinfovalidator.web.model.Option;
import rc.apps.tradeinfovalidator.web.model.ResponseSuccess;
import rc.apps.tradeinfovalidator.web.model.Spot;

@RestController
@RequestMapping("/validation")
public class TradeInfoValidationRestService {
	
	@PostMapping("/spot")
	public ResponseSuccess validateSpot(@RequestBody @Valid Spot spot) {
		System.out.println("validation success");
		System.out.println(spot);
		return new ResponseSuccess("success");
	}

	@PostMapping("/option")
	public ResponseSuccess validateOption(@RequestBody @Valid Option option) {
	    System.out.println("validation success");
	    System.out.println(option);
	    return new ResponseSuccess("success");
	}

	@PostMapping("/forward")
	public ResponseSuccess validateForward(@RequestBody @Valid Forward forward ) {
	    System.out.println("validation success");
	    System.out.println(forward);
	    return new ResponseSuccess("success");
	}
	
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<FieldError> handleException(MethodArgumentNotValidException exception) {
    	System.out.println("handling error");
        List<FieldError> fieldErrors= exception.getBindingResult().getFieldErrors();
        return fieldErrors;
    }
	
}
