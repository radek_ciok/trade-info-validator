package rc.apps.tradeinfovalidator.web.validation.rules;

import java.util.ArrayList;
import java.util.List;

public enum SupportedStyles {
    AMERICAN, EUROPEAN;
    
    public static boolean isSupported(String styleName) {
        for (SupportedStyles style : values()) {
            if (styleName.equals(style.name())) {
                return true;
            }
        }
        return false;
    }
    
    public static SupportedStyles getStyleByName(String styleName) {
        for (SupportedStyles style : values()) {
            if (styleName.equals(style.name())) {
                return style;
            }
        }        
        return null;
    }
    
    public List<String> getSupportedStyleNames() {
        List<String> names = new ArrayList<String>();
        for (SupportedStyles style : values()) {
            names.add(style.name());
        }
        return names;
    }
}
