package rc.apps.tradeinfovalidator.web.validation.rules;

import java.util.Currency;

public class IsValidISOCodeRule extends TradeInfoValidationRule {

	private String currencies;
	
	public IsValidISOCodeRule(String fieldName, String currencies) {
		super(fieldName);
		this.currencies = currencies;
	}

	@Override
	public boolean isBroken() {
	    
		if (currencies.length()!=6) {
			return true;
		}
		
		String currency1 = currencies.substring(0, 3);
		String currency2 = currencies.substring(3, 6);

		boolean isCurrency1Valid = false;
	    boolean isCurrency2Valid = false;
	    
	    for (Currency availableCurrency : Currency.getAvailableCurrencies()) {
	        if (!isCurrency1Valid && availableCurrency.getCurrencyCode().equals(currency1)) {
	            isCurrency1Valid = true;
	        }
            if (!isCurrency2Valid && availableCurrency.getCurrencyCode().equals(currency2)) {
                isCurrency2Valid = true;
            }	        
        }
		return !(isCurrency1Valid && isCurrency2Valid);
	}

	@Override
	public String getMessage() {
		return "This is not valid ISO 4217 currency code";
	}
	

}
