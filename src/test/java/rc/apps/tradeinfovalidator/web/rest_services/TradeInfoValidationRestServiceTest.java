package rc.apps.tradeinfovalidator.web.rest_services;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(value = TradeInfoValidationRestService.class)
public class TradeInfoValidationRestServiceTest {

	@Autowired
	private MockMvc mvc;

	@Test
    public void spotValidationSuccess() throws Exception {
		String jsonStr = "{\"customer\":\"JUPITER1\",\"ccyPair\":\"EURUSD\",\"type\":\"Spot\",\"direction\":\"BUY\",\"tradeDate\":\"2016-08-11\",\"amount1\":1000000.00,\"amount2\":1120000.00,\"rate\":1.17,\"valueDate\":\"2017-06-07\",\"legalEntity\":\"CS London\",\"trader\":\"Johann Baumfiddler\"}";
        mvc.perform(post("/validation/spot")
                        .content(jsonStr)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

	@Test
    public void spotValidationFails_BreakeAllRules() throws Exception {
		String jsonStr = "{\"customer\":\"JUPITER3\","
				+ "\"ccyPair\":\"EURUSX\","
				+ "\"type\":\"Spot\","
				+ "\"direction\":\"BUY\","
				+ "\"tradeDate\":\"2017-06-18\","
				+ "\"amount1\":1000000.00,"
				+ "\"amount2\":1120000.00,"
				+ "\"rate\":1.17,"
				+ "\"valueDate\":\"2017-06-17\","
				+ "\"legalEntity\":\"CS London\","
				+ "\"trader\":\"Johann Baumfiddler\"}";
        mvc.perform(post("/validation/spot")
                        .content(jsonStr)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[*].defaultMessage", 
                		containsInAnyOrder(
                				"This is not valid ISO 4217 currency code",
                				"This counterparty is not supported",
                				"Date have to be a working day",
                				"Value Date have to be after Trade Date",
                				"Value date for this type have to be 2 days after current date"
                				)));    
    }
	
	
	@Test
    public void forwardValidationSuccess() throws Exception {
		String jsonStr = "{\"customer\":\"JUPITER2\",\"ccyPair\":\"EURUSD\",\"type\":\"Forward\",\"direction\":\"SELL\",\"tradeDate\":\"2016-08-11\",\"amount1\":1000000.00,\"amount2\":1120000.00,\"rate\":1.17,\"valueDate\":\"2016-08-22\",\"legalEntity\":\"CS London\",\"trader\":\"Johann Baumfiddler\"}";
        mvc.perform(post("/validation/forward")
                        .content(jsonStr)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

	@Test
    public void forwardValidationFails_BreakeAllRules() throws Exception {
		String jsonStr = 
				"{\"customer\":\"JUPITER3\","
				+ "\"ccyPair\":\"AAAUSD\","
				+ "\"type\":\"Forward\","
				+ "\"direction\":\"SELL\","
				+ "\"tradeDate\":\"2016-08-11\","
				+ "\"amount1\":1000000.00,"
				+ "\"amount2\":1120000.00,"
				+ "\"rate\":1.17,"
				+ "\"valueDate\":\"2015-01-04\","
				+ "\"legalEntity\":\"CS London\","
				+ "\"trader\":\"Johann Baumfiddler\"}";
        mvc.perform(post("/validation/forward")
                        .content(jsonStr)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[*].defaultMessage", 
                		containsInAnyOrder(
                				"This is not valid ISO 4217 currency code",
                				"This counterparty is not supported",
                				"Date have to be a working day",
                				"Value Date have to be after Trade Date"
                				)));    
    }

	@Test
    public void optionValidationSuccess() throws Exception {
		String jsonStr = "{\"customer\":\"JUPITER1\","
				+ "\"ccyPair\":\"EURUSD\","
				+ "\"type\":\"VanillaOption\","
				+ "\"style\":\"AMERICAN\","
				+ "\"direction\":\"BUY\","
				+ "\"strategy\":\"CALL\","
				+ "\"tradeDate\":\"2016-08-11\","
				+ "\"amount1\":1000000.00,"
				+ "\"amount2\":1120000.00,"
				+ "\"rate\":1.17,"
				+ "\"deliveryDate\":\"2016-08-22\","
				+ "\"expiryDate\":\"2016-08-19\","
				+ "\"excerciseDate\":\"2016-08-12\","
				+ "\"payCcy\":\"USD\","
				+ "\"premium\":0.20,"
				+ "\"premiumCcy\":\"USD\","
				+ "\"premiumType\":\"%USD\","
				+ "\"premiumDate\":\"2016-08-12\","
				+ "\"legalEntity\":\"CS London\","
				+ "\"trader\":\"Johann Baumfiddler\"}";
        mvc.perform(post("/validation/option")
                        .content(jsonStr)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

	@Test
    public void optionValidationFails_NotSupportedStyle() throws Exception {
		String jsonStr = 
				"{\"customer\":\"JUPITER1\","
				+ "\"ccyPair\":\"EURUSD\","
				+ "\"type\":\"VanillaOption\","
				+ "\"style\":\"AMERICAn\","
				+ "\"direction\":\"BUY\","
				+ "\"strategy\":\"CALL\","
				+ "\"tradeDate\":\"2016-08-11\","
				+ "\"amount1\":1000000.00,"
				+ "\"amount2\":1120000.00,"
				+ "\"rate\":1.17,"
				+ "\"deliveryDate\":\"2016-08-22\","
				+ "\"expiryDate\":\"2016-08-19\","
				+ "\"excerciseDate\":\"2016-08-12\","
				+ "\"payCcy\":\"USD\","
				+ "\"premium\":0.20,"
				+ "\"premiumCcy\":\"USD\","
				+ "\"premiumType\":\"%USD\","
				+ "\"premiumDate\":\"2016-08-12\","
				+ "\"legalEntity\":\"CS London\","
				+ "\"trader\":\"Johann Baumfiddler\"}";
        mvc.perform(post("/validation/option")
                        .content(jsonStr)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[*].defaultMessage", 
                		containsInAnyOrder("This style is not supported")));    
    }

	@Test
    public void optionValidationFails_BreakeAllRules() throws Exception {
		String jsonStr = 
				"{\"customer\":\"JUPITER\","
				+ "\"ccyPair\":\"EURUSx\","
				+ "\"type\":\"VanillaOption\","
				+ "\"style\":\"AMERICAN\","
				+ "\"direction\":\"BUY\","
				+ "\"strategy\":\"CALL\","
				+ "\"tradeDate\":\"2016-08-11\","
				+ "\"amount1\":1000000.00,"
				+ "\"amount2\":1120000.00,"
				+ "\"rate\":1.17,"
				+ "\"deliveryDate\":\"2016-08-22\","
				+ "\"expiryDate\":\"2017-08-19\","
				+ "\"excerciseDate\":\"2015-08-12\","
				+ "\"payCcy\":\"USD\","
				+ "\"premium\":0.20,"
				+ "\"premiumCcy\":\"USD\","
				+ "\"premiumType\":\"%USD\","
				+ "\"premiumDate\":\"2016-08-12\","
				+ "\"legalEntity\":\"CS London\","
				+ "\"trader\":\"Johann Baumfiddler\"}";
        mvc.perform(post("/validation/option")
                        .content(jsonStr)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", hasSize(4)))
                .andExpect(jsonPath("$[*].defaultMessage", 
                		containsInAnyOrder(
                				"This is not valid ISO 4217 currency code",
                				"American style requires excerciseDate, which is after the trade date and before the expiry date",
                				"This counterparty is not supported",
                				"Expiry date and premium date shall be before delivery date"
                				)));    
    }
	
}
