package rc.apps.tradeinfovalidator.web.model;

public enum TradeInfoType {
	Spot, Forward, VanillaOption
}
