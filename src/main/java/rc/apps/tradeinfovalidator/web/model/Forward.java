package rc.apps.tradeinfovalidator.web.model;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import rc.apps.tradeinfovalidator.web.validation.ValidForward;

@ValidForward
public class Forward extends CommonTradeInfo {
	
	@NotNull
	private LocalDate valueDate;
		
	public LocalDate getValueDate() {
		return valueDate;
	}
	public void setValueDate(LocalDate valueDate) {
		this.valueDate = valueDate;
	}
	@Override
	public String toString() {
		return "Forward [valueDate=" + valueDate + ", getCustomer()=" + getCustomer() + ", getCcyPair()=" + getCcyPair()
				+ ", getType()=" + getType() + ", getDirection()=" + getDirection() + ", getTradeDate()="
				+ getTradeDate() + ", getAmount1()=" + getAmount1() + ", getAmount2()=" + getAmount2() + ", getRate()="
				+ getRate() + ", getLegalEntity()=" + getLegalEntity() + ", getTrader()=" + getTrader()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	
	
}
